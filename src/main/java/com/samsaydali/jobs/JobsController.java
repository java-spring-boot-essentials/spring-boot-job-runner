package com.samsaydali.jobs;

import org.jobrunr.scheduling.JobScheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.bind.DefaultValue;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;


@RestController
@RequestMapping("/jobs")
public class JobsController {

    @Autowired
    private JobScheduler jobScheduler;

    @Autowired
    private SampleJobService sampleJobService;

    @GetMapping("/enqueue/{input}")
    public String enqueue(@PathVariable @DefaultValue("default-input") String input) {
        jobScheduler.enqueue(() -> {sampleJobService.executeSampleJob(input);});

        return "ok";
    }


    @GetMapping("/schedule/{input}")
    public String schedule(
            @PathVariable @DefaultValue("default-input") String input,
            @RequestParam("scheduleAt") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime scheduleAt
    ) {
        jobScheduler.schedule(scheduleAt, () -> {sampleJobService.executeSampleJob(input);});

        return "ok";
    }
}
